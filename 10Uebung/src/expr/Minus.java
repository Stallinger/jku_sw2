package expr;

import expr.visitor.ExprVisit;

/**
 * Class for minus unary expressions. 
 */
public class Minus extends UnExpr {

	/**
	 * Constructor setting the subexpression. 
	 * @param expr the subexpression
	 */
	Minus(Expr expr) {
		super(expr);
	}
	
	/*
	 * (non-Javadoc)
	 * @see expr.Expr#accept(expr.visitor.ExprVisit)
	 */
	public <T> T accept(ExprVisit<T> exprVisit) {
		return exprVisit == null ? null : exprVisit.visit(this);
	}

}
