package expr;

import expr.visitor.ExprVisit;

/**
 * Interface for expressions. 
 * Expressions should allow visitors.  
 */
public interface Expr {
	
	/**
	 * accept method
	 * @param exprVisit
	 * @return result of visitor
	 */
	public <T> T accept(ExprVisit<T> exprVisit);
}
