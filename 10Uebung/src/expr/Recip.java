package expr;

import expr.visitor.ExprVisit;

/**
 * Class for reciprocal unary expressions. 
 */
public class Recip extends UnExpr {

	/**
	 * Constrctor setting the subexpression of this reciprocal expression. 
	 * @param expr the subexpression
	 */
	Recip(Expr expr) {
		super(expr);
	}

	/*
	 * (non-Javadoc)
	 * @see expr.Expr#accept(expr.visitor.ExprVisit)
	 */
	public <T> T accept(ExprVisit<T> exprVisit) {
		return exprVisit == null ? null : exprVisit.visit(this);
	}
}
