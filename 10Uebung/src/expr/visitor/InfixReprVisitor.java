package expr.visitor;

import expr.Add;
import expr.Exprs;
import expr.Lit;
import expr.Minus;
import expr.Mult;
import expr.Recip;
import expr.Var;

/**
 * transfered the expression into a string
 * @author Stalli
 *	
 */
public class InfixReprVisitor implements ExprVisit<String> {

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Add)
	 */
	public String visit(Add add) {
		return "(" + add.getLeft().accept(this) + " + " + add.getRight().accept(this) + ")";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Lit)
	 */
	public String visit(Lit lit) {
		return String.valueOf(lit.getVal());
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Minus)
	 */
	public String visit(Minus minus) {
		return "(-" + minus.getSubExpr().accept(this) + ")";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Mult)
	 */
	public String visit(Mult mult) {
		return "(" + mult.getLeft().accept(this) + " * " + mult.getRight().accept(this) + ")";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Recip)
	 */
	public String visit(Recip recip) {
		return "(1/" + recip.getSubExpr().accept(this) + ")";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Var)
	 */
	public String visit(Var var) {
		return var.getName();
	}

}
