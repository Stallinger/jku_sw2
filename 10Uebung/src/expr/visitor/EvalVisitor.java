package expr.visitor;

import expr.Add;
import expr.Lit;
import expr.Minus;
import expr.Mult;
import expr.Recip;
import expr.Var;

/**
 * calculate the expression
 * @author Stalli
 */
public class EvalVisitor implements ExprVisit<Double> {

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Add)
	 */
	public Double visit(Add add) {
		return add.getLeft().accept(this) + add.getRight().accept(this);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Lit)
	 */
	public Double visit(Lit lit) {
		return lit.getVal();
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Minus)
	 */
	public Double visit(Minus minus) {
		return minus.getSubExpr().accept(this)*-1;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Mult)
	 */
	public Double visit(Mult mult) {
		return mult.getLeft().accept(this) * mult.getRight().accept(this);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Recip)
	 */
	public Double visit(Recip recip) {
		return 1.0 / recip.getSubExpr().accept(this);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Var)
	 */
	public Double visit(Var var) {
		return var.getValue();
	}
}
