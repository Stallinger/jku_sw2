package expr.visitor;

import expr.Add;
import expr.Expr;
import expr.Exprs;
import expr.Lit;
import expr.Minus;
import expr.Mult;
import expr.Recip;
import expr.Var;

/**
 * simplifies the expression
 * @author Stalli
 *
 */
public class SimplifyVisitor implements ExprVisit<Expr> {

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Add)
	 */
	public Expr visit(Add add) {
		Add a = Exprs.add(add.getLeft().accept(this), add.getRight().accept(this));
		
		if(a.getLeft() instanceof Lit) {
			if(((Lit)a.getLeft()).getVal() == 0) return a.getRight();
		} 
		if(a.getRight() instanceof Lit) {
			if(((Lit)a.getRight()).getVal() == 0) return a.getLeft();
		} 
		return a;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Lit)
	 */
	public Expr visit(Lit lit) {
		return lit;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Minus)
	 */
	public Expr visit(Minus minus) {
		Minus mi = Exprs.minus(minus.getSubExpr().accept(this));
		if(mi.getSubExpr() instanceof Minus) {
			Minus m = (Minus)mi.getSubExpr();
			return m.getSubExpr().accept(this);
		}
		return mi.getSubExpr().accept(this);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Mult)
	 */
	public Expr visit(Mult mult) {
		Mult m = Exprs.mult(mult.getLeft().accept(this), mult.getRight().accept(this));
		
		if(m.getLeft() instanceof Lit) {
			double d = ((Lit)m.getLeft()).getVal();
			if(d == 0) return Exprs.lit(0);
			if(d == 1) return m.getRight().accept(this);
		} 
		if(m.getRight() instanceof Lit) {
			double d = ((Lit)m.getRight()).getVal();
			if(d == 0) return Exprs.lit(0);
			if(d == 1) return m.getLeft().accept(this);
		} 
		return m;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Recip)
	 */
	public Expr visit(Recip recip) {
		Recip r = Exprs.recip(recip.getSubExpr().accept(this));

		if(r.getSubExpr() instanceof Recip) {
			Recip sub = (Recip) r.getSubExpr();
			return sub.getSubExpr();
		}
		
		return r;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see expr.visitor.ExprVisit#visit(expr.Var)
	 */
	public Expr visit(Var var) {
		return var;
	}

}
