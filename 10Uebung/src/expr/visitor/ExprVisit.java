package expr.visitor;

import expr.*;

/**
 * visitor interface
 * @author Stalli
 *
 * @param <T>
 * 
 * <p>see</p>
 * <ul>
 * <li>EvalVisitor</li>
 * <li>InfixReprVisitor</li>
 * <li>SimplifyVisitor</li>
 * </ul>
 */
public interface ExprVisit<T> {

	/**
	 * visitor method for expr.Add
	 * @param add
	 * @return
	 */
	T visit(Add add);
	/**
	 * visitor method for expr.Lit
	 * @param lit
	 * @return
	 */
	T visit(Lit lit);
	/**
	 * visitor method for expr.Minus
	 * @param minus
	 * @return
	 */
	T visit(Minus minus);
	/**
	 * visitor method for expr.Mult
	 * @param mult
	 * @return
	 */
	T visit(Mult mult);
	/**
	 * visitor method for expr.Recip
	 * @param recip
	 * @return
	 */
	T visit(Recip recip);
	/**
	 * visitor method for expr.Var
	 * @param var
	 * @return
	 */
	T visit(Var var);
	
}
