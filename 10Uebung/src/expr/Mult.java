package expr;

import expr.visitor.ExprVisit;

/**
 * Class for multiplication expressions. 
 */
public class Mult extends BinExpr {
	
	/**
	 * Constructor for the multiplication expression with left and right subexpressions. 
	 * @param left the left subexpression
	 * @param right the right subexpression
	 */
	Mult(Expr left, Expr right) {
		super(left, right);
	}

	/*
	 * (non-Javadoc)
	 * @see expr.Expr#accept(expr.visitor.ExprVisit)
	 */
	public <T> T accept(ExprVisit<T> exprVisit) {
		return exprVisit == null ? null : exprVisit.visit(this);
	}
}
