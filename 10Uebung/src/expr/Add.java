package expr;

import expr.visitor.ExprVisit;

/**
 * Class for add expressions. 
 */
public class Add extends BinExpr {

	/**
	 * Constructor for the add expression with left and right subexpressions. 
	 * @param left the left subexpression
	 * @param right the right subexpression
	 */
	Add(Expr left, Expr right) {
		super(left, right);
	}

	/*
	 * (non-Javadoc)
	 * @see expr.Expr#accept(expr.visitor.ExprVisit)
	 */
	public <T> T accept(ExprVisit<T> exprVisit) {
		return exprVisit == null ? null : exprVisit.visit(this);
	}
}
