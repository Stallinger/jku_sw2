package mdraw.ui.tools;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.ImageIcon;

import mdraw.model.ShapeModel;
import mdraw.shapes.Shape;
import mdraw.shapes.ShapeUtil;

/**
 * Tool implementation for handling selections. Either the tool selects objects,
 * moves selected objects, or resizes a single selected object.
 * 
 * @author hp
 * @version 1.1
 * @since 1.0
 */
@SuppressWarnings("serial")
public class SelTool extends Tool {

	/** Enumeration for encoding the different selection states of this tool */
	private enum SelState {
		IDLE, SELECTING
	}

	/** The shape model */
	private final ShapeModel model;

	/** The current state of this tool */
	private SelState state = SelState.IDLE;

	/** Mouse position representing the start of a dragging operation */
	private Point startPoint;

	/** Mouse position representing the end position of a selection action */
	private Point selectionPoint;

	/** Selected shapes by current selection gesture */
	private Shape[] selection;

	/**
	 * Constructor initializing tool palette and shape model
	 * 
	 * @param palette
	 *            the tool palette
	 * @param model
	 *            the shape model
	 */
	public SelTool(ToolPalette palette, ShapeModel model) {
		super("Sel", new ImageIcon("sel.png"), palette);
		this.model = model;
		putValue(Action.SHORT_DESCRIPTION, "Selection tool");
	}

	// selection

	/**
	 * Handles mouse clicked events (forwarded from drawing panel) by finding a
	 * selected shape and making it the single selected shape or, if shift down
	 * is pressed, changes the selection of this shape (select when not selected,
	 * deselect when is selected).
	 * 
	 * @param me
	 *            the mouse event object
	 */
	@Override
	public void mouseClicked(MouseEvent me) {
		for (Shape shape : model.getShapes()) {
			if (shape.isSelection(me.getX(), me.getY())) {
				if (me.isShiftDown()) {
					changeSelection(shape);
				} else {
					model.setSelection(new Shape[] { shape });
				}
				return;
			}
		}
		// no shape selected
		model.clearSelection();
	}

	// move, resize, select

	/**
	 * Handles mouse pressed events (forwarded from drawing panel). Finds out
	 * the selected element. Initiates a resizing, moving or selection process
	 * dependent on the selection.
	 * 
	 * @param me
	 *            the mouse event object
	 */
	@Override
	public void mousePressed(MouseEvent me) {
		if (model.getSelected().length == 0 || me.isShiftDown()) {
			state = SelState.SELECTING;
			startPoint = me.getPoint();
			selectionPoint = me.getPoint();
			selection = new Shape[0];
		} else {
			state = SelState.IDLE;
		}
	}

	/**
	 * Handles mouse dragged events (forwarded from drawing panel). Dependent of
	 * the state of this tool will either show resizing, moving, or selecting
	 * shapes. Does not actually execute the operation but will show a visual
	 * feedback.
	 * 
	 * @param me
	 *            the mouse event object
	 */
	@Override
	public void mouseDragged(MouseEvent me) {
		if (state == SelState.IDLE) {
			return;
		}
		if (state == SelState.SELECTING) {
			selectionPoint = me.getPoint();
			Shape[] oldSelection = selection;
			selection = ShapeUtil.getSelected(model.getShapes(), startPoint.x,
					startPoint.y, selectionPoint.x, selectionPoint.y);
			for (Shape s : oldSelection) {
				changeSelection(s);
			}
			for (Shape s : selection) {
				changeSelection(s);
			}
		}
	}

	/**
	 * Handles mouse released events (forwarded from drawing panel). Dependent
	 * of the state of this tool will either resize a selected shape, move
	 * selected shape, or change the selections of shapes.
	 * 
	 * @param me
	 *            the mouse event object
	 */
	@Override
	public void mouseReleased(MouseEvent me) {
		if (state == SelState.IDLE) {
			return;
		}
		if (state == SelState.SELECTING) {
			state = SelState.IDLE;
		}
	}

	/**
	 * Changes the selection state of a shape. That means, when not selected will
	 * select the shape, when selected will deselect the shape.
	 * 
	 * @param s
	 *            the shape to change the selections state.
	 */
	private void changeSelection(Shape s) {
		if (model.isSelected(s)) {
			model.removeSelection(s);
		} else {
			model.addSelection(s);
		}
	}

}
