package mdraw.shapes;

import java.awt.Color;
import java.awt.Graphics;

public class ColorDecorator extends Decorator {

	private final Color color;
	
	public ColorDecorator(Color color, Shape next) {
		super(next);
		this.color = color;
	}
	
	public Color getColor() {
		return color;
	}
  
	@Override
	public void draw(Graphics g) {
		super.draw(g);
		fill(g, color);
	}
	
}
