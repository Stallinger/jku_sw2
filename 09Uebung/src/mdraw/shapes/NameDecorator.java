package mdraw.shapes;

import java.awt.Graphics;

public class NameDecorator extends Decorator {

	private final String name;
	
	public NameDecorator(String name, Shape next) {
		super(next);
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public void draw(Graphics g) {
		super.draw(g);
		g.drawString(name, getLeft(), getTop() - 5);
	}

}
