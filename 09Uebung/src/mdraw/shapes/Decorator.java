package mdraw.shapes;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Decorator implements Shape {

		private final Shape next;
		
		public Decorator(Shape next) {
			this.next = next;
		}
		
		public Shape getNext() {
			return next;
		}
		
		public int getLeft() {
			return next.getLeft();
		}

		public int getTop() {
			return next.getTop();
		}

		public int getWidth() {
			return next.getWidth();
		}

		public int getHeight() {
			return next.getHeight();
		}

		public void setPos(int x, int y) {
			next.setPos(x, y);
		}

		public void setSize(int w, int h) {
			next.setSize(w, h);
		}

		public void draw(Graphics g) {
			next.draw(g);
		}

		public void fill(Graphics g, Color c) {
			next.fill(g, c);
		}

		public boolean isSelection(int x, int y) {
			return next.isSelection(x, y);
		}
}
