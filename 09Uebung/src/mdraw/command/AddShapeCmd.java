package mdraw.command;

import mdraw.model.ShapeModel;
import mdraw.shapes.Shape;

public class AddShapeCmd extends AbstractCommand {

	private final Shape shape;
	
	public AddShapeCmd(ShapeModel model,Shape shape) {
		super(model);
		this.shape = shape;
	}
	
	@Override
	public void doIt() {
		model.addShape(shape);
	}

	@Override
	public void undo() {
		model.removeShape(shape);
	}

}
