package mdraw.command;

import mdraw.model.ShapeModel;
import mdraw.shapes.Group;
import mdraw.shapes.Shape;

public class UngroupShapeCmd extends AbstractCommand {

	private final Group group;
	
	public UngroupShapeCmd(ShapeModel model,Group group) {
		super(model);
		this.group = group;
	}

	@Override
	public void doIt() {
		model.removeShape(group);
		for(Shape s : group.getElements()) {
			model.addShape(s);
		}
	}

	@Override
	public void undo() {
		model.addShape(group);
		for(Shape s : group.getElements()) {
			model.removeShape(s);
		}
	}

}
