package mdraw.command;

import mdraw.model.ShapeModel;
import mdraw.shapes.Shape;

public class RemoveShapesCmd extends AbstractCommand {

	private final Shape[] shapes;

	public RemoveShapesCmd(ShapeModel model,Shape... shapes) {
		super(model);
		this.shapes = shapes;
	}
	
	@Override
	public void doIt() {
		for(Shape s : shapes) {
			model.removeShape(s);
		}
	}

	@Override
	public void undo() {
		for(Shape s: shapes) {
			model.addShape(s);
		}
	}

}
