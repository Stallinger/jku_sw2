package mdraw.command;

import mdraw.model.ShapeModel;

public abstract class AbstractCommand implements Command {

	protected final ShapeModel model;
	
	public AbstractCommand(ShapeModel model) {
		this.model = model;
	}
	
	@Override
	public void redo() {
		doIt();
	}
}
