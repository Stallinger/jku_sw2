package mdraw.command;

import mdraw.model.ShapeModel;
import mdraw.shapes.Group;
import mdraw.shapes.Shape;

public class GroupShapeCmd extends AbstractCommand {

	private final Group group;
	
	public GroupShapeCmd(ShapeModel model,Group group) {
		super(model);
		this.group = group;
	}

	@Override
	public void doIt() {
		for (Shape s : group.getElements()) {
			model.removeShape(s);
		}
		model.addShape(group);
	}

	@Override
	public void undo() {
		for (Shape s : group.getElements()) {
			model.addShape(s);
		}
		model.removeShape(group);
	}

}
