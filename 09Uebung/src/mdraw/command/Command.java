package mdraw.command;

public interface Command {
	public void doIt();
	
	public void undo();
	
	public void redo();
}
