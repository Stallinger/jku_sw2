package mdraw.command;

import mdraw.model.ShapeModel;
import mdraw.shapes.Decorator;


public class UndecorShapesCmd extends AbstractCommand {

	private final Decorator[] decorators;

	public UndecorShapesCmd(ShapeModel model,Decorator... decorators) {
		super(model);
		this.decorators = decorators;
	}
	
	@Override
	public void doIt() {
		for(Decorator d : decorators) {
			model.removeShape(d);
			model.addShape(d.getNext());
		}
	}

	@Override
	public void undo() {
		for(Decorator d : decorators) {
			model.removeShape(d.getNext());
			model.addShape(d);
		}
	}

}
