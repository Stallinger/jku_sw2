package mdraw.command;

import mdraw.model.ShapeModel;
import mdraw.shapes.*;

public class DecorateShapesCmd extends AbstractCommand {
	private final Decorator[] decorators;
	
	public DecorateShapesCmd(ShapeModel model, Decorator... decorators) {
		super(model);
		this.decorators = decorators;
	}
	
	@Override
	public void doIt() {
		for(Decorator d : decorators) {
			model.removeShape(d.getNext());
			model.addShape(d);
		}
	}

	@Override
	public void undo() {
		for(Decorator d : decorators) {
			model.removeShape(d);
			model.addShape(d.getNext());
		}
	}
}
