package mdraw.command;

import java.util.ArrayDeque;
import java.util.Deque;

public class CommandHandler {

	private static CommandHandler instance; 
	
	public static CommandHandler getInstance() {
		if (instance == null) {
			instance = new CommandHandler(); 
		}
		return instance; 
	}
	
	private final Deque<Command> undoStack;
	private final Deque<Command> redoStack;
	
	private CommandHandler() {
		super(); 
		this.undoStack = new ArrayDeque<>();
		this.redoStack = new ArrayDeque<>();
	}
	
	public void doCommand(Command cmd) {
		cmd.doIt();
		undoStack.addFirst(cmd);
		redoStack.clear();
	}

	public void undoCommand() {
		if (undoStack.isEmpty()) {
			return;
		}
		Command cmd = undoStack.getFirst();
		cmd.undo();
		undoStack.removeFirst();
		redoStack.addFirst(cmd);
	}

	public void redoCommand() {
		if (redoStack.isEmpty()) {
			return;
		}
		Command cmd = redoStack.removeFirst();
		cmd.redo();
		undoStack.addFirst(cmd);
	}
	
}
