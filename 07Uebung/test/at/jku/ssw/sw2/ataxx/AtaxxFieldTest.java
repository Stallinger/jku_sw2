package at.jku.ssw.sw2.ataxx;

import at.jku.ssw.sw2.ataxx.AtaxxField;
import at.jku.ssw.sw2.ataxx.State;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static at.jku.ssw.sw2.ataxx.State.*;
import static org.junit.Assert.fail;

public class AtaxxFieldTest {
    private AtaxxField field;

    @Before
    public void setUp() {
        field = new AtaxxField();
    }

    // TODO in general add checks for score and current player where you see fit

    @Test
    public void testInitialState() {
        // example use of the method assertFieldEquals, feel free to use it
        assertFieldEquals(new State[][]{
                {ONE, NIL, NIL, NIL, NIL, NIL, TWO},
                {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                {TWO, NIL, NIL, NIL, NIL, NIL, ONE},
        });
    }

    @Test
    public void testValidMove() {
        // TODO check if the field is modified as expected, when a player make a valid move, i.e., adding a stone to one's one neighbor stone
    	field.set(0, 0, 1, 0);
        field.set(6, 0, 6, 1);
        assertFieldEquals(new State[][]{
            {ONE, NIL, NIL, NIL, NIL, NIL, TWO},
            {ONE, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {TWO, TWO, NIL, NIL, NIL, NIL, ONE},
        });
    }

    @Test
    public void testValidJump() {
        // TODO check if the field is modified as expected when a player makes valid jump, i.e., a move of a stone by 2 rows or columns
        field.set(0, 0, 0, 2);
        field.set(6, 0, 6, 2);
        assertFieldEquals(new State[][]{
            {NIL, NIL, ONE, NIL, NIL, NIL, TWO},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, TWO, NIL, NIL, NIL, ONE},
        });
    }

    @Test
    public void testValidOrder() {
        // TODO check if the field works as expected, when players make valid moves in the right order, i.e., take turns
    	field.set(0, 0, 0, 2);
        field.set(6, 0, 6, 2);
        field.set(0, 2, 0, 3);
        field.set(0,6, 0, 4);
      
        assertFieldEquals(new State[][]{
            {NIL, NIL, ONE, TWO, TWO, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, TWO, NIL, NIL, NIL, ONE},
        });
    }
    
    @Test
    public void testOccupy() {
        // TODO check if the field works as expected, when players make valid moves in the right order, i.e., take turns
    	field.set(0, 0, 2, 2);
        field.set(0, 6, 1, 6);
        field.set(2, 2, 2, 4);
        field.set(0, 6, 0, 5);
        field.set(2, 4, 1, 5);
      
        assertFieldEquals(new State[][]{
            {NIL, NIL, NIL, NIL, NIL, ONE, ONE},
            {NIL, NIL, NIL, NIL, NIL, ONE, ONE},
            {NIL, NIL, NIL, NIL, ONE, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {NIL, NIL, NIL, NIL, NIL, NIL, NIL},
            {TWO, NIL, NIL, NIL, NIL, NIL, ONE},
        });
    }

    @Test (expected = IllegalStateException.class)
    public void testInvalidOrder() {
        // TODO check if the field enforces the players to take turns
    	field.set(0, 0, 0, 2);
        field.set(6, 0, 6, 2);
        field.set(0, 2, 0, 3);
        field.set(0,6, 0, 4);
        field.set(0,4, 1, 4);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInvalidMove() {
        // TODO check if the field throws the expected exceptions, when invalid moves are made
    	field.set(8, 0, 0, 2);
    }
    
    private void assertFieldEquals(State[][] expectedField) {
        for (int row = 0; row < AtaxxField.SIZE; ++row) {
            for (int column = 0; column < AtaxxField.SIZE; ++column) {
                Assert.assertEquals(
                        String.format("wrong state in field [%d][%d]", row, column),
                        expectedField[row][column],
                        field.get(row, column));
            }
        }
    }
}
