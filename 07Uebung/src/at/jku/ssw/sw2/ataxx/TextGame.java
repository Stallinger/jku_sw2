package at.jku.ssw.sw2.ataxx;

import inout.In;
import inout.Out;

public class TextGame {
    public static void main(String[] args) {
        final AtaxxField field = new AtaxxField();

        while(!field.isGameOver()) {
        	Out.println(field);
        	Out.println("Score player O: " + field.getScore(State.ONE));
        	Out.println("Score player X: " + field.getScore(State.TWO));
        	Out.println("It's the turn of player: " + field.getNextPlayer());
        	Out.print("Please enter your source and target positions, row and column for each (anything but a number to quit): ");
        	int fromRow = In.readInt();
        	int fromColumn = In.readInt();
        	int toRow = In.readInt();
        	int toColumn = In.readInt();
        	
        	if(!In.done()) {
        		return;
        	}
        	
        	try {
        		field.set(fromRow, fromColumn, toRow, toColumn);
        	} catch(Exception exc) {
        		Out.println(exc.getMessage());
        	}
        	
        	Out.println();
        }
        Out.println("GameOver");
        Out.println("Winner is: " + field.getWinner().toString());
    }
}
