package at.jku.ssw.sw2.ataxx;

import java.util.Arrays;

public class AtaxxField {
    public static final int SIZE = 7;

    public State[][] grid = new State[SIZE][SIZE];
    private State lastplayer = null;
    
    public AtaxxField() {
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			grid[i][j]= State.NIL;
    		}
    	}
    	grid[0][0] = State.ONE;
    	grid[SIZE -1][SIZE -1] = State.ONE;
    	grid[0][SIZE -1] = State.TWO;
    	grid[SIZE -1][0] = State.TWO;
    }
    
    public void set(int fromRow,int fromColumn,int toRow,int toColumn) {
    	if(fromRow < 0 || fromRow >= SIZE || fromColumn < 0 || fromColumn >= SIZE || 
    			toRow < 0 || toRow >= SIZE || toColumn < 0 || toColumn >= SIZE)
    		throw new IllegalArgumentException("index out of bounds");
    	if(fromRow == toRow && fromColumn == toColumn)
    		throw new IllegalArgumentException("source position = target position");
    	if(Math.abs(fromRow - toRow) >= 3 || Math.abs(fromColumn - toColumn) >= 3) 
    		throw new IllegalArgumentException("wrong target position");
    	
    	if(grid[fromRow][fromColumn] == State.NIL)
    		throw new IllegalStateException("wrong source position");
    	if(grid[fromRow][fromColumn] != getNextPlayer())
    		throw new IllegalStateException("wrong player");
    	if(grid[toRow][toColumn] != State.NIL)
    		throw new IllegalStateException("target position is not empty");
    	
    	if(Math.abs(fromRow - toRow) >= 2 || Math.abs(fromColumn - toColumn) >= 2) {
    		grid[toRow][toColumn] = grid[fromRow][fromColumn];
    		grid[fromRow][fromColumn] = State.NIL;
    	} else {
    		grid[toRow][toColumn] = grid[fromRow][fromColumn];
    	}
    		
    	changeFieldState(toRow + 1, toColumn);
    	changeFieldState(toRow + 1, toColumn + 1);
    	changeFieldState(toRow + 1, toColumn - 1);
    	changeFieldState(toRow - 1, toColumn);
    	changeFieldState(toRow - 1, toColumn + 1);
    	changeFieldState(toRow - 1, toColumn - 1);
    	changeFieldState(toRow, toColumn + 1);
    	changeFieldState(toRow, toColumn - 1);
    	
    	lastplayer = getNextPlayer();
    }
    
    private void changeFieldState(int row,int column) {
    	if(row >= 0 && row < SIZE && column >= 0 && column < SIZE ) {
    		if(grid[row][column] != State.NIL && grid[row][column] != getNextPlayer()) {
    			grid[row][column] = getNextPlayer();
    		}
    	}
    }
    
    public State get(int row, int column) {
    	if(row >= 0 && row < SIZE && column >= 0 && column < SIZE )
    		return grid[row][column];
    	else
    		throw new IllegalArgumentException("index out of bounds");
    }
    
    public State getNextPlayer() {
    	return (lastplayer == null || lastplayer == State.TWO) ? State.ONE : State.TWO; 
    }
    
    public int getScore(State state) {
    	int sum = 0;
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			if(state == grid[i][j]) {
    				sum++;
    			}
    		}
    	}
    	return sum;
    }
    
    public boolean isGameOver() {
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			if(grid[i][j] == State.NIL)
    				return false;
    		}
    	}
    	return true;
    }
    
    public State getWinner() {
    	if(!isGameOver())
    		return State.NIL;
    	else
    		return (getScore(State.ONE) > getScore(State.TWO)) ? State.ONE : State.TWO;
    }
    
    @Override
    public String toString() {
    	String str = " |0|1|2|3|4|5|6|";
    	
    	for(int i = 0; i < SIZE;i++) {
    		str += "\r\n" + i;
    		
    		for(int j = 0; j < SIZE;j++) {
    			str+= "|" + grid[i][j];
    		}
    		str+="|";
    	}
    	return str;
    }
}
