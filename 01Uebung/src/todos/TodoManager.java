package todos;

import java.time.LocalDate;

public class TodoManager {

	private final TodoList todolist;
	
	public TodoManager(){
		todolist = new TodoList();
	}
	
	public void add(String description, LocalDate date){
		todolist.insertTodo(new Todo(description,date));
	}
	
	public void add(String description, int year, int month, int day){
		this.add(description, LocalDate.of(year, month, day));
	}
	
	public Todo getTodoById(int id){
		TodoNode node = todolist.firstEntry();
		while(node != null){
			if(node.todo.getId() == id)
				return node.todo;
			node = node.next;
		}
		return null;
	}
	
	public Todo[] getUntil(LocalDate date){
		return getUntil(todolist,date);
	}
	
	private Todo[] getUntil(TodoList list,LocalDate date){
		TodoNode node = list.firstEntry();
		int sum = 0;
		while(node != null){
			if(node.todo.getMaturityDate().isBefore(date) ||
					node.todo.getMaturityDate().isEqual(date)){
				sum++;
			}
			node = node.next;
		}
		return getTodos(sum,list);
	}
	
	public Todo[] getDoneTodos(boolean done){
		TodoList list =  getDoneTodoList(done);
		return getTodos(list.getCount(),list);
	}
	
	private TodoList getDoneTodoList(boolean done){
		TodoList doneTodos = new TodoList();
		TodoList openTodos = new TodoList();		
		TodoNode node = todolist.firstEntry();
	
		while(node != null){
			if(node.todo.getStatus() == Status.DONE){
				doneTodos.insertTodo(node.todo);
			} else {
				openTodos.insertTodo(node.todo);
			}
			node = node.next;
		}
	
		if (done){
			return doneTodos;
		} else {
			return openTodos;
		}
	}
	
	public Todo[] getDoneUntil(LocalDate date, boolean done){
		TodoList list = getDoneTodoList(done);
		return getUntil(list,date);
	}
	
	public void deleteUntil(LocalDate date){
		TodoNode node = todolist.firstEntry();
		while(node != null){
			if((node.todo.getMaturityDate().isBefore(date) ||
					node.todo.getMaturityDate().isEqual(date)) &&
					node.todo.getStatus() == Status.DONE){
				todolist.deleteNode(node);
			}
			node = node.next;
		}
	}
	
	public Todo[] getAllTodos(){
		return getTodos(todolist.getCount(),todolist);
	}
	
	private Todo[] getTodos(int count, TodoList list){
		Todo[] todos = new Todo[count];
		TodoNode node = list.firstEntry();
		
		for(int i = 0; i < count;i++){
			todos[i] = node.todo;
			node = node.next;
		}
		return todos;
	}
}
