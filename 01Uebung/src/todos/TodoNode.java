package todos;

class TodoNode {
	
	Todo todo;
	TodoNode next;
	
	TodoNode(Todo todo, TodoNode next) {
		this.todo = todo;
		this.next = next;
	}

	TodoNode(Todo todo) {
		this(todo, null);
	}
}
