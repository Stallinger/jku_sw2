package todos;

import java.time.LocalDate;

public class Todo {
	
	private static int nextId = 0;
	
	private final int id;
	private final String description;
	private final LocalDate maturityDate;
	private Status status;
	
	public Todo(String description, LocalDate maturityDate){
		this.id = nextId;
		nextId++;
		
		this.description = description;
		this.maturityDate = maturityDate;
		this.status = Status.OPEN;
	}
	
	public Todo(String description, int year, int month, int dayOfMonth){
		this(description, LocalDate.of(year, month, dayOfMonth));
	}
	
	public void setDone(){
		this.status = Status.DONE;
	}
	
	public int getId(){
		return id;
	}
	
	public LocalDate getMaturityDate(){
		return maturityDate;
	}
	
	public String getDescription(){
		return description;
	}
	
	public Status getStatus(){
		return status;
	}
	
	@Override
	public String toString() {
		String returnStr = String.format("%2d", getId()) + ": " + 
				getMaturityDate() + " - " + String.format("%1$-30s", getDescription()) + ": ";
		
		if(getStatus() == Status.DONE) {
			returnStr += "DONE";
		} else {
			returnStr += "OPEN";
		}
		
		return returnStr;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((maturityDate == null) ? 0 : maturityDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Todo other = (Todo) obj;
		if(id != other.id)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (maturityDate == null) {
			if (other.maturityDate != null)
				return false;
		} else if (!maturityDate.equals(other.maturityDate))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
}
