package todos.app;

import java.time.LocalDate;

import inout.In;
import inout.Out;
import todos.Todo;
import todos.TodoManager;

public class TodosMain {

	public static void main(String[] args) {
		TodoManager myTodoManager = new TodoManager();
		
		In.open("todos.txt");
		if (!In.done()) {
			Out.println("Cannot open file todos.txt"); 
			return; 
		}
		
		int year = In.readInt(); 
		while (In.done()) {
			int month = In.readInt(); 
			int day = In.readInt(); 
			String descr = In.readString(); 
			
			myTodoManager.add(descr, year, month, day);
			
			year = In.readInt();
		}
		In.close(); 
		
		Out.println("All Todos");
		Out.println("---------");
		printTodos(myTodoManager.getAllTodos());
		Out.println("");
		Out.println("Todos Until March 09");
		Out.println("--------------------");
		printTodos(myTodoManager.getUntil(LocalDate.of(2017,3, 9)));
		
		myTodoManager.getTodoById(2).setDone();
		myTodoManager.getTodoById(4).setDone();
		myTodoManager.getTodoById(1).setDone();
		myTodoManager.getTodoById(5).setDone();
		
		Out.println("");
		Out.println("Done");
		Out.println("----");
		printTodos(myTodoManager.getDoneTodos(true));
		Out.println("");
		Out.println("Still Open");
		Out.println("----------");
		printTodos(myTodoManager.getDoneTodos(false));
		Out.println("");
		Out.println("Still Open Until March 09");
		Out.println("-------------------------");
		printTodos(myTodoManager.getDoneUntil(LocalDate.of(2017,3, 9), false));
		
		myTodoManager.deleteUntil(LocalDate.of(2017,3, 9));	
		
		Out.println("");
		Out.println("All Todos");
		Out.println("---------");
		printTodos(myTodoManager.getAllTodos());
	}
	
	private static void printTodos(Todo[] todos){
		for(Todo t : todos){
			Out.println(t);
		}
	}

}
