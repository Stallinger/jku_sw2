package todos;

import java.time.LocalDate;

class TodoList {

	private TodoNode head;
	private int count;
	
	TodoList(){
		head = null;
		count = 0;
	}
	
	TodoNode firstEntry() {
		return head; 
	} 
	
	void deleteNode(TodoNode deleteNode){
		if(firstEntry().equals(deleteNode)){
			head = head.next;
			count--;
		}
		
		TodoNode node = firstEntry();
		while(node.next != null){
			if (node.next.equals(deleteNode)){
				node.next = node.next.next;
				count--;
			}
		    node = node.next;
		}
	}
	
	void insertTodo(String description,LocalDate maturitydate) {
		insertTodo(new Todo(description,maturitydate));
	}
	
	void insertTodo(Todo todo){
		TodoNode pred = null;
		TodoNode curr = head;
		
		while (curr != null
				&& curr.todo.getMaturityDate().isBefore(todo.getMaturityDate())) {
			pred = curr;
			curr = curr.next;
		} 
		TodoNode node = new TodoNode(todo);
		
		if (pred == null) { 
			head = node;
		} else {
			pred.next = node;
		}
		node.next = curr;
		count++;
	}
	
	int getCount(){
		return count;
	}
}
