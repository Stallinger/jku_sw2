/*
 * Lukas STALLINGER
 * k1648098
 */
package blocks;

public interface Renderable {
	void render(int x,int y);
	void render();
}
