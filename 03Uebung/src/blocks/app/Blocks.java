/*
 * Lukas STALLINGER
 * k1648098
 */
package blocks.app;

import blocks.*;

public class Blocks {
	
	public static Block line(String string) {
		return new TextBlock(string);
	}
	
	public static Block lines(Block...blocks) {
		return new RowBlock(blocks);
	}
	
	public static Block lines(String...strings) {
		TextBlock [] tb = new TextBlock[strings.length];
		for(int i = 0;i < strings.length;i++) {
			tb[i] = new TextBlock(strings[i]);
		}
		return new RowBlock(tb);
	}
	
	public static Block cols(Block...blocks) {
		return new ColumBlock(blocks);
	}
	
	public static void main(String[] args) {
		lines(   
			cols(    
				lines("1st col 1st line", "1st col 2nd line", "1st col 3rd line"),    
				lines("2nd col 1st line", "2nd col 2nd line")),    
			line("This line is at the very bottom. ")  
		).render(20,20);
		
		cols(   
			lines(    
				lines("left side", "left side 2nd line", "left side 3rd line"),    
				lines("extra box left side", "extra box left side 2nd line")),    
			lines(line("right side "),
				lines(line("extra box on the right side"),
					line("extra box left side 2nd line"),
			cols(    
					lines("1st col 1st line", "1st col 2nd line", "1st col 3rd line"),    
					lines("2nd col 1st line", "2nd col 2nd line"))))  
		).render(50,210);
		
		lines(line("mega line"),
				cols(lines("colum1"),
						lines("colum2"),
						lines("colum3","colum3 second line"),
						lines("colum4"),
						lines("colum5")),
				line("second mega line"),
				line("third mega line"),
				cols(lines("no mega line","no mega line second line"),lines("no mega line"))
		).render(400,50);
		
		lines(line("my box"),
				line("my box second line"),
				cols(lines("col1","col1 second line"),lines("col2","col2 second line")),
				cols(lines("colum1"),
						lines("colum2"),
						lines("colum3","colum3 second line"),
						lines("colum4"),
						lines("colum5")),
				cols(lines("colum1"),
						lines("colum2"),
						lines("colum3","colum3 second line"),
						lines("colum4"),
						lines("colum5"))
		).render(400,380);
	}
}
