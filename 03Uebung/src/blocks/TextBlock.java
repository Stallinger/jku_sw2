/*
 * Lukas STALLINGER
 * k1648098
 */
package blocks;

import inout.Window;

public class TextBlock extends Block{
	private final String text;
	
	public TextBlock(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	@Override
	public void render(int x, int y) {
		 Window.drawText(text,x,y);
	}

	@Override
	public void render() {
		Window.drawText(text,0,0);
	}

	@Override
	public int getWidth() {
		return Window.getTextWidth(text);
	}

	@Override
	public int getHeight() {
		return Window.getTextHeight();
	}
}
