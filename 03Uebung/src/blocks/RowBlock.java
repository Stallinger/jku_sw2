/*
 * Lukas STALLINGER
 * k1648098
 */
package blocks;

import java.awt.Color;

import inout.Window;

public class RowBlock extends Block {
	private final Block[] rectangles;
	
	public RowBlock(Block...rectangles) {
		this.rectangles = rectangles;
	}
	
	@Override
	public void render(int x, int y) {
		int akY = y;
		for(int i = 0; i < rectangles.length;i++) {
			rectangles[i].render(x + PADDING, akY + PADDING);
			akY = akY + rectangles[i].getHeight();
		}	
		Window.drawRectangle(x, y, getWidth()  - PADDING, getHeight() - PADDING, Color.LIGHT_GRAY);
	}

	@Override
	public void render() {
		int x = 0;
		int y = 0;
		for(int i = 0; i < rectangles.length;i++) {
			rectangles[i].render(x, y);
			y = y + rectangles[i].getHeight();
		}	
		Window.drawRectangle(0, 0, getWidth(), getHeight(), Color.LIGHT_GRAY);
	}

	@Override
	public int getWidth() {
		int width = 0;
		for(Block r : rectangles) {
			if(width < r.getWidth())
				width = r.getWidth();
		}
		return width + (PADDING*2);
	}

	@Override
	public int getHeight() {
		int height = 0;
		for(Block r : rectangles) {
			height += r.getHeight();
		}
		return height + (PADDING*2);
	}
}
