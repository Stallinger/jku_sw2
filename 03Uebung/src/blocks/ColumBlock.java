/*
 * Lukas STALLINGER
 * k1648098
 */
package blocks;

import java.awt.Color;

import inout.Window;

public class ColumBlock extends Block {
	private final Block[] rectangles;
	
	public ColumBlock(Block...rectangles) {
		this.rectangles = rectangles;
	}
	
	@Override
	public void render(int x, int y) {
		int akX = x;
		for(int i = 0; i < rectangles.length;i++) {
			rectangles[i].render(akX + PADDING, y + PADDING);
			akX = akX + rectangles[i].getWidth();
		}	
		Window.drawRectangle(x, y, getWidth() - PADDING, getHeight() - PADDING, Color.LIGHT_GRAY);
	}

	@Override
	public void render() {
		int x = 0;
		int y = 0;
		for(int i = 0; i < rectangles.length;i++) {
			rectangles[i].render(x, y);
			x = x + rectangles[i].getWidth();
		}	
		Window.drawRectangle(0, 0, getWidth(), getHeight(), Color.LIGHT_GRAY);
	}

	@Override
	public int getWidth() {
		int width = 0;
		for(Block r : rectangles) {
			width += r.getWidth();
		}
		return width + (PADDING*2);
	}

	@Override
	public int getHeight() {
		int height = 0;
		for(Block r : rectangles) {
			if(height < r.getHeight())
				height = r.getHeight();
		}
		return height + (PADDING*2);
	}
}
