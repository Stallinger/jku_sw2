/*
 * Lukas STALLINGER
 * k1648098
 */
package blocks;

public abstract class Block implements Renderable{
	final static int PADDING = 4;
	
	public abstract int getWidth();
	public abstract int getHeight();
}
