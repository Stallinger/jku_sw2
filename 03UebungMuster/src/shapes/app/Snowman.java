package shapes.app;

import inout.Out;
import shapes.Circle;
import shapes.Group;
import shapes.Rectangle;
import shapes.Shape;

/**
 * Test class constructing a snowman as composite object
 */
public class Snowman {

	public static void main(String args[]) {

		Shape hut1 = new Rectangle(130, 120, 40, 10);
		Shape hut2 = new Rectangle(140, 90, 20, 30);
		Group hut = new Group(hut1, hut2);

		Rectangle beinL = new Rectangle(100, 300, 45, 100);
		Rectangle beinR = new Rectangle(155, 300, 45, 100);
		Rectangle armL = new Rectangle(60, 210, 40, 30);
		Rectangle armR = new Rectangle(200, 210, 40, 30);
		Circle body = new Circle(150, 250, 60);
		Circle kopf = new Circle(150, 160, 30);

		Group snowMan = new Group(hut, armL, armR, beinR, beinL, kopf, body);

		snowMan.draw();
		
		Out.println(snowMan.getLeft()); 
	}
}
