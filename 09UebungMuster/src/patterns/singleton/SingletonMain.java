package patterns.singleton;

public class SingletonMain {

	public static void main(String[] args) {
		
		Singleton.getInstance().useSingleton();
		Singleton.getInstance().useSingleton();
		
	}

}
