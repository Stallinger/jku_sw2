package patterns.command;

public interface Command {

	public void doCmd();
	
	public void undoCmd();
	
}
