package shapes;

import shapes.visitor.ShapeVisitor;

/**
 * Class representing circular shape.
 */
public class Circle extends Primitive {
	
	/** Radius of the circle. */
    private final int radius;

    /**
     * Constructor initializing position and radius.
     * 
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     * @param radius the radius of this circle
     */
    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    /** 
     * Gets the radius of this circle.
     * 
     * @return the radius
     */
    public int getRadius() {
        return radius;
    }
    
	/**
	 * @see shapes.Shape#getLeft()
	 */
	@Override
	public int getLeft() {
		return getX() - radius;
	}

	/**
	 * @see shapes.Shape#getTop()
	 */
	@Override
	public int getTop() {
		return getY() - radius;
	}

	/**
	 * @see shapes.Shape#getWidth()
	 */
	@Override
	public int getWidth() {
		return radius * 2;
	}

	/**
	 * @see shapes.Shape#getHeight()
	 */
	@Override
	public int getHeight() {
		return radius * 2;
	}

	/**
	 * @see Shape#accept(ShapeVisitor)
	 */
	@Override
	public void accept(ShapeVisitor v) {
		v.visit(this);
	}

}
