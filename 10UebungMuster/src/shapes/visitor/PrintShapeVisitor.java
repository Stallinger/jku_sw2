package shapes.visitor;

import shapes.Circle;
import shapes.Group;
import shapes.Rect;
import shapes.Shape;

public class PrintShapeVisitor implements ShapeVisitor {
	private int openGroups = 0;

	@Override
	public void visit(Circle c) {
		printGroupIndent();
		System.out.printf("Circle %d/%d radius %d%n",
				c.getX(), c.getY(), c.getRadius());
	}

	@Override
	public void visit(Rect r) {
		printGroupIndent();
		System.out.printf("Rect pos=%d/%d w/h %d%n",
				r.getLeft(), r.getTop(), r.getWidth(), r.getHeight());
	}

	@Override
	public void visit(Group g) {
		printGroupIndent();
		System.out.println("--------- Group with subelements --------");
		++openGroups;
		for (Shape s : g.getSubshapes()) {
			s.accept(this);
		}
		--openGroups;
		printGroupIndent();
		System.out.println("-----------------------------------------");
	}

	private void printGroupIndent() {
		for (int i = 0; i < openGroups; ++i) {
			System.out.print("  ");
		}
	}
}
