package shapes.app;

import shapes.Circle;
import shapes.Group;
import shapes.Rect;
import shapes.Shape;
import shapes.visitor.AreaShapeVisitor;
import shapes.visitor.DrawShapeVisitor;
import shapes.visitor.PrintShapeVisitor;

/**
 * Test class constructing a snowman as composite object
 */
public class Snowman {

	public static void main(String args[]) {

		Shape hat1 = new Rect(130, 120, 40, 10);
		Shape hat2 = new Rect(140, 90, 20, 30);
		Group hat = new Group(hat1, hat2);

		Rect legLeft = new Rect(100, 300, 45, 100);
		Rect legRight = new Rect(155, 300, 45, 100);
		Rect armLeft = new Rect(60, 210, 40, 30);
		Rect armRight = new Rect(200, 210, 40, 30);
		Circle body = new Circle(150, 250, 60);
		Circle head = new Circle(150, 160, 30);

		Group snowMan = new Group(hat, armLeft, armRight, legRight, legLeft, head, body);

		snowMan.accept(new DrawShapeVisitor());
		
		snowMan.accept(new PrintShapeVisitor());
		
		AreaShapeVisitor areaVisitor = new AreaShapeVisitor();
		snowMan.accept(areaVisitor);
		
		System.out.println("Area of snowman = " + areaVisitor.getArea());
		
	}
}
