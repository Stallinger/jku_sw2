package phonebook;

/**
 * Class implementing the nodes for the linear list of phone book entries
 */
final class EntryNode {
	
	/** the entry value */
	Entry entry;
	
	/** the next node */
	EntryNode next;

	/**
	 * Constructor for setting entry and next node
	 * @param entry the entry value
	 * @param next the next node reference
	 */
	EntryNode(Entry entry, EntryNode next) {
		this.entry = entry;
		this.next = next;
	}

	/**
	 * Constructor for setting entry
	 * @param entry the entry value
	 */
	EntryNode(Entry entry) {
		this(entry, null);
	}

} 
