package grading;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import inout.Out;

import static inout.Out.*;

public class GradingSW2 {
	
	public static void main(String[] args) throws IOException {
		// TODO: 1. List<Data> 
		Out.println("1. List of Data Objects");
		Out.println("-----------------------");
		List<Data> data = java.nio.file.Files.lines(java.nio.file.Paths.get("data.csv"))
			.skip(1)
			.map(s -> s.split(";", -1))
			.map(s -> {
				return new Data(s[1],s[2],s[3],s[4],
						Arrays.asList(s)
							.subList(5, 5+11)
							.stream()
							.map(x -> Integer.parseInt(x))
							.collect(Collectors.toCollection(LinkedList::new)),
						Arrays.asList(s)
							.subList(16, 16 + 11)
							.stream()
							.filter(x -> !x.isEmpty())
							.map(x -> Double.parseDouble(x))
							.collect(Collectors.toCollection(LinkedList::new)),
						(!s[27].isEmpty()) ? Double.parseDouble(s[27]) : -1.0);
				})
			.sorted(Comparator.comparing(s -> s.name))
			.collect(Collectors.toCollection(LinkedList::new));
		
		data.forEach(Out::println);
		
		// TODO: 2. List<Result>   
		Out.println();
		Out.println("2. List of detailed results");
		Out.println("---------------------------");
		List<Result> results = data.stream()
			.map(d -> {
				return new Result(d.name,d.firstName,d.stdtId,d.skz,
					d.submitted
						.stream()
						.mapToInt(Integer::intValue)
						.sum(),
					d.points
						.stream()
						.collect(Collectors.averagingDouble(Double::doubleValue)),
					d.test);
				})
			.collect(Collectors.toCollection(LinkedList::new));

		results.forEach(Out::println);
		
		// TODO: 3. List<Grading>
		Out.println();
		Out.println("3. Gradings");
		Out.println("-----------");
		List<Grading> gradings = results.stream()
			.filter(r -> r.submitted >= 9)
			.filter(r -> r.test != -1)
			.map(r -> {
				Grade grade = Grade.NotPassed;
				if(r.test >= 12) {
					double g = ((r.test + r.avrgPoints) / 2.0) * ((double)r.submitted / 11.0);
					if(g >= 21) 	grade = Grade.Excellent;
					else if(g > 18) grade = Grade.Good;
					else if(g > 15) grade = Grade.Satisfying;
					else 			grade = Grade.Sufficient;
				}
				return new Grading(r.stdtId,r.skz,grade);
				})
			.collect(Collectors.toCollection(LinkedList::new));
		
		gradings.forEach(Out::println);
		
		// TODO: 4. statistics of grades 
		Out.println();
		Out.println("4. Statistics");
		Out.println("-------------");
		
		Map<Grade, List<Grading>> statistic = gradings.stream().collect(Collectors.groupingBy(g -> g.grade));
		Out.println("Grade 1: " + statistic.get(Grade.Excellent).size());
		Out.println("Grade 2: " + statistic.get(Grade.Good).size());
		Out.println("Grade 3: " + statistic.get(Grade.Satisfying).size());
		Out.println("Grade 4: " + statistic.get(Grade.Sufficient).size());
		Out.println("Grade 5: " + statistic.get(Grade.NotPassed).size());
		
		// TODO 5. csv-file 
		Out.println();
		Out.println("5. CSV File");
		Out.println("-----------");
		String csvFile = gradings.stream()
			.map(g -> (String.format("%1s;%2s;%1d",g.stdtId,g.skz,(int)g.grade.ordinal() + 1)))
			.collect(Collectors.joining("\r\n"));
		
		Out.println(csvFile);
		Out.open("grades.csv");
		Out.print(csvFile);
		Out.close();
	}
}
	
class Data {
	final String name; 
	final String firstName; 
	final String stdtId; 
	final String skz; 
	final List<Integer> submitted; 
	final List<Double> points;
	final double test;
	
	public Data(String name, String firstName, String stdtId, String skz, List<Integer> submitted, List<Double> points, double test) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.stdtId = stdtId; 
		this.skz = skz; 
		this.submitted = submitted;
		this.points = points;
		this.test = test; 
	}

	@Override
	public String toString() {
		return "Data [name=" + name + ", firstName=" + firstName + ", stdtId=" + stdtId + ", skz=" + skz
				+ ", submitted=" + submitted + ", points=" + points + ", test=" + test + "]";
	}
}

class Result {
	final String name; 
	final String firstName; 
	final String stdtId; 
	final String skz; 
	final int submitted; 
	final double avrgPoints;
	final double test;
	
	public Result(String name, String firstName, String stdtId, String skz, int submitted, double avrgPoints, double test) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.stdtId = stdtId;
		this.skz = skz; 
		this.submitted = submitted;
		this.avrgPoints = avrgPoints;
		this.test = test;
	}

	@Override
	public String toString() {
		return "Result [name=" + name + ", firstName=" + firstName + ", stdtId=" + stdtId + ", skz=" + skz
				+ ", submitted=" + submitted + ", avrgPoints=" + avrgPoints + ", test=" + test + "]";
	}	
}

enum Grade {
	Excellent, Good, Satisfying, Sufficient, NotPassed;	
}

class Grading {
	final String stdtId; 
	final String skz; 
	final Grade grade;
	
	public Grading(String stdtId, String skz, Grade grade) {
		super();
		this.stdtId = stdtId;
		this.skz = skz; 
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Grading [stdtId=" + stdtId + ", skz=" + skz + ", grade=" + grade + "]";
	}
}
