package at.jku.ssw.sw2.ataxx;

public enum State {
    NIL(" "), ONE("blue"), TWO("red");
    
    private String alias;
    
    State(String alias) {
    	this.alias = alias;
    }
    
    @Override
    public String toString() {
    	return alias;
    }
}
