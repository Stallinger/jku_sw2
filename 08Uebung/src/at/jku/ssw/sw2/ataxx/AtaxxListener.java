package at.jku.ssw.sw2.ataxx;


import java.util.EventListener;

public interface AtaxxListener extends EventListener {
	
	public void ataxxChanged(AtaxxEvent e);
}
