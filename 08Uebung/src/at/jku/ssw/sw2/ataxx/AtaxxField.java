package at.jku.ssw.sw2.ataxx;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class AtaxxField {
    static public final int SIZE = 7;

    private final State[][] grid = new State[SIZE][SIZE];
    private final List<AtaxxListener> ataxxListeners = new CopyOnWriteArrayList<>();
    private State lastplayer = null;
    private int indexX = -1;
    private int indexY = -1;
    
    
    public AtaxxField() {
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			grid[i][j]= State.NIL;
    		}
    	}
    	grid[0][0] = State.ONE;
    	grid[SIZE -1][SIZE -1] = State.ONE;
    	grid[0][SIZE -1] = State.TWO;
    	grid[SIZE -1][0] = State.TWO;
    }
    
    private void changeFieldState(int row,int column) {
    	if(row >= 0 && row < SIZE && column >= 0 && column < SIZE ) {
    		if(grid[row][column] != State.NIL && grid[row][column] != getNextPlayer()) {
    			grid[row][column] = getNextPlayer();
    		}
    	}
    }
    
    public State get(int row, int column) {
    	if(row >= 0 && row < SIZE && column >= 0 && column < SIZE )
    		return grid[row][column];
    	else
    		throw new IllegalArgumentException("index out of bounds");
    }
    
    public State getNextPlayer() {
    	return (lastplayer == null || lastplayer == State.TWO) ? State.ONE : State.TWO; 
    }
    
    public int getScore(State state) {
    	int sum = 0;
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			if(state == grid[i][j]) {
    				sum++;
    			}
    		}
    	}
    	return sum;
    }
    
    public boolean isGameOver() {
    	int sumOne = 0,sumTwo = 0;
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			if(grid[i][j] == State.ONE) 		sumOne++;
    			else if(grid[i][j] == State.TWO) 	sumTwo++;
    		}
    	}
    	
    	if(sumOne == 0 || sumTwo == 0)
    		return true;
    	
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			if(tryMove(i,j)) return false;
    		}
    	}
    	
  
    	for(int i = 0; i < SIZE;i++) {
    		for(int j = 0; j < SIZE;j++) {
    			if(grid[i][j] == State.NIL)
    				return false;
    		}
    	}
    	return true;
    }
    
    private boolean tryMove(int x, int y) {
    	for(int i = -2; i <= 2;i++) {
    		for(int j = -2; j <= 2;j++) {
    			try {
    				if(State.NIL == get(x+i,y+j)) //possible move
    					return true;
    			} catch(Exception exc) { }
    		}
    	}
    	return false;
    }
    
    
    public State getWinner() {
    	if(!isGameOver())
    		return State.NIL;
    	else
    		return (getScore(State.ONE) > getScore(State.TWO)) ? State.ONE : State.TWO;
    }
    
    public boolean isSelected() {
    	return (indexX != -1 && indexY != -1);
    }
    
    public void select(int x,int y) {
    	if(x < 0 || x >= SIZE || y < 0 || y >= SIZE)
    		throw new IllegalArgumentException("index out of bounds");
    	
    	if(grid[x][y] == State.NIL)
    		throw new IllegalStateException("wrong source position");
    	if(grid[x][y] != getNextPlayer())
    		throw new IllegalStateException("wrong player");
    	
    	if(!tryMove(x,y))
    		throw new IllegalStateException("no possible move");
    	
    	indexX = x;
    	indexY = y;
    }
    
    public void move(int x,int y) {
    	if(isSelected()) {
    		if(x < 0 || x >= SIZE || y < 0 || y >= SIZE)
        		throw new IllegalArgumentException("index out of bounds");
    		if(grid[x][y] != State.NIL)
        		throw new IllegalStateException("target position is not empty");
    		if(x == indexX && y == indexY)
        		throw new IllegalArgumentException("source position = target position");
        	if(Math.abs(x - indexX) >= 3 || Math.abs(y - indexY) >= 3) 
        		throw new IllegalArgumentException("wrong target position");
    		
        	if(Math.abs(indexX - x) >= 2 || Math.abs(indexY - y) >= 2) {
        		grid[x][y] = grid[indexX][indexY];
        		grid[indexX][indexY] = State.NIL;
        	} else {
        		grid[x][y] = grid[indexX][indexY];
        	}
        		
        	changeFieldState(x + 1, y);
        	changeFieldState(x + 1, y + 1);
        	changeFieldState(x + 1, y - 1);
        	changeFieldState(x - 1, y);
        	changeFieldState(x - 1, y + 1);
        	changeFieldState(x - 1, y - 1);
        	changeFieldState(x, y + 1);
        	changeFieldState(x, y - 1);
        	
        	lastplayer = getNextPlayer();
        	
    		indexX = -1;
    		indexY = -1;
    		fireAtaxxEvent();
    	} else {
    		throw new IllegalStateException("select first");
    	}
    }
    
    public void addAtaxxListener(AtaxxListener ataxxListener) {
    	ataxxListeners.add(ataxxListener);
    }
    
    public void removeAtaxxListener(Object ataxxListener) {
    	ataxxListeners.remove(ataxxListener);
    }
    
    protected void fireAtaxxEvent() {
    	final AtaxxEvent e = new AtaxxEvent(this,getWinner());
    	for(AtaxxListener a : ataxxListeners) {
    		a.ataxxChanged(e);
    	}
    }
    
}
