package at.jku.ssw.sw2.ataxx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class JAtaxxField extends JComponent {

	private final AtaxxField ataxxField;
	private final MouseListener mouseListener = new MouseAdapter(){
		@Override
		public void mouseReleased(MouseEvent e) {
			super.mouseReleased(e);

			int row = (int)(e.getX() / getFieldWidth());
			int col = (int)(e.getY() / getFieldHeight());
			
			try{
				if(ataxxField.isSelected()) {
					ataxxField.move(row, col);
				} else {
					ataxxField.select(row, col);
				}
			} catch(Exception exc) {
				JOptionPane.showMessageDialog(null,
						exc.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	};
	
	public JAtaxxField(AtaxxField ataxxField) {
		this.ataxxField = ataxxField;
		this.addMouseListener(mouseListener);
		this.ataxxField.addAtaxxListener(e -> repaint());
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g.create();
		
		float fieldWidth = getFieldWidth();
		float fieldHeight = getFieldHeight();
		
		for(int i = 0; i < AtaxxField.SIZE;i++) {
			for(int j = 0; j < AtaxxField.SIZE;j++) {
				State state = ataxxField.get(i, j);
				
				if(state == State.ONE) {
					g2d.setColor(Color.blue);
					g2d.fillRoundRect((int)(i * fieldWidth), (int)(j * fieldHeight),(int) fieldWidth,(int)fieldHeight,50,50);
				} else if(state == State.TWO) {
					g2d.setColor(Color.red);
					g2d.fillOval((int)(i * fieldWidth), (int)(j * fieldHeight),(int) fieldWidth,(int)fieldHeight);
				}
			}
		}
	}
	
	private float getFieldWidth() {
		return  (float)getWidth() / (float) AtaxxField.SIZE;
	}
	
	private float getFieldHeight() {
		return (float) getHeight() / (float) AtaxxField.SIZE;
	}
	
}
