package at.jku.ssw.sw2.ataxx;

import java.awt.BorderLayout;

import javax.swing.*;

public class WindowGame {

	private final static AtaxxField ataxxField = new AtaxxField();
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> initialize(ataxxField));
	}	
	
	private static void initialize(AtaxxField ataxxField) {
		JFrame frame = new JFrame("AtaxxGame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 800);
		frame.add(new JAtaxxField(ataxxField),BorderLayout.CENTER);
		
		
		JLabel label = new JLabel("Score: 2:2");
		ataxxField.addAtaxxListener(new AtaxxListener() {
			@Override
			public void ataxxChanged(AtaxxEvent e) {
				if(e.getWinner() != State.NIL) {
					JOptionPane.showMessageDialog(frame,
							"Winner is: " + e.getWinner(), "Information",
							JOptionPane.INFORMATION_MESSAGE);
					frame.dispose();
				}
				
				label.setText("Score: " + ataxxField.getScore(State.ONE) + ":" +
						ataxxField.getScore(State.TWO));
			}
		});
		frame.add(label,BorderLayout.SOUTH);
		frame.setVisible(true);
	}

}
