package at.jku.ssw.sw2.ataxx;

import java.util.EventObject;

@SuppressWarnings("serial")
public class AtaxxEvent extends EventObject{

	private final State winner;
	
	public AtaxxEvent(Object arg0, State winner) {
		super(arg0);
		this.winner = winner;
	}
	
	public final State getWinner() {
		return winner;
	}

}
