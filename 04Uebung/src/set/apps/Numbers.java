package set.apps;

import set.SortedSet;

public class Numbers {

	public static void main(String[] args) {
		SortedSet<Integer> mySortedSet = new SortedSet<Integer>((a, b) -> a - b);

		mySortedSet.addAll(new Integer[]{42, 1, 12, 200, 12, 12, 30 });
		
		System.out.println("Elements");
		System.out.println("--------");
		
		mySortedSet.forEach((s) -> System.out.println(s));
		
		mySortedSet.remove(1);
		mySortedSet.remove(200);
		mySortedSet.remove(30);
		
		System.out.println("\nRemaining Elements");
		System.out.println("------------------");
		
		mySortedSet.forEach((s) -> System.out.println(s));
		
		System.out.println("\nCurrent Size");
		System.out.println("------------");
		System.out.println(mySortedSet.getSize());
		
	}

}
