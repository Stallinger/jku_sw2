package set.apps;

import set.SortedSet;


public class Students {
	private static class Student {
		private final String name;
		private final int age;
		
		Student(String name, int age) {
			this.name = name;
			this.age = age;
		}
	}
	
	public static void main(String[] args) {
		SortedSet<Student> students = new SortedSet<Student>((a,b) -> (a.age - b.age));
		students.addAll(new Student[]{
				new Student("A",22),
				new Student("B",18),
				new Student("C",19),
				new Student("D",25)		
		});
		
		System.out.println("Students:");
		System.out.println("---------");
		students.forEach((s) -> System.out.println(s.name + " " + s.age));
		
		SortedSet<Student> oldStudents = students.filter((s) -> (s.age > 20));	
		
		System.out.println("\nOld Students:");
		System.out.println("-------------");
		oldStudents.forEach((s) -> System.out.println(s.name + " " + s.age));
		
		Integer sum = students.reduce(0,(i,s) -> (i += s.age));
		System.out.println("\nSum student age: " + sum);
		
		Integer max = students.reduce(0,(i,s) -> 
			{if (i < s.age)return i = s.age;else return i; });
		System.out.println("\nMax student age: " + max);
		
		SortedSet<String> names = students.map((s) -> (s.name),
				(a,b) -> (a.compareTo(b)));
		
		System.out.println("\nNames:");
		System.out.println("------");
		names.forEach((n) -> System.out.println(n));
		
	}
}
