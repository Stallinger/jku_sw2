package set;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class SortedSet<T> implements Iterable<T> {
	protected static class Node<T> {
		final T value;
		Node<T> next;

		Node(T elem) {
			this(elem,null);
		}
		Node(T elem,Node<T> next) {
			this.value = elem;
			this.next = next;
		}
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			private Node<T> current;
			
			{
				current = head;
			}
	
			@Override
			public boolean hasNext() {
				return current != null;
			}
	
			@Override
			public T next() {
				if (current != null) {
					T value = current.value;
					current = current.next;
					return value;
				} else {
					throw new NoSuchElementException("No more element in list");
				}
			}
		};
	}
	
	protected int size = 0;
	public int getSize(){ 
		return size;
	}
	
	protected Node<T> head = null;
	protected final Comparator<T> comparator;
	
	public SortedSet(Comparator<T> comparator) {
		this.comparator = comparator;
	}
	
	public boolean contains(T elem) {
		if(elem == null) return false;
		for(T t : this) {
			if(comparator.compare(t,elem) == 0)
				return true;
		}
		return false;
	}
	
	public boolean add(T elem ) {
		if(contains(elem) || elem == null) return false;	
		
		Node<T> pred = null, first = head;
		
		
		while(first != null && comparator.compare(first.value, elem) < 0) {
			pred = first;
			first = first.next;
		}
		
		Node<T> node = new Node<T>(elem);
		
		if (pred == null) {
			head = node;
		} else {
			pred.next = node;
		}
		node.next = first;

		size++;
		return true;
	}
	
	public void addAll(T[] ts) {
		for(T t : ts) {
			add(t);
		}
	}
	
	public boolean remove(Object elem) {
		if(elem == null) return false;
		int size = this.size;
		
		Node<T> first = head;
		if(first.value.equals(elem)){
			head = head.next;
			this.size--;
		}
		
		while(first != null && first.next != null) {
			if(first.next.value.equals(elem)) {
				first.next = first.next.next;
				this.size--;
			}
			first = first.next;
		}
		
		return size != this.size;
	}
	
	public SortedSet<T> filter(Predicate<T> test) {
		SortedSet<T> newSet = new SortedSet<>(comparator);
		for(T t : this) {
			if(test.test(t)) {
				newSet.add(t);
			}
		}
		return newSet;
	}
	
	public <U> SortedSet<U> map(Function<T, U> fn, Comparator<U> comparator) {
		SortedSet<U> newSet = new SortedSet<>(comparator);
		for(T t : this) {
			newSet.add(fn.apply(t));
		}
		return newSet;
	}
	
	public <U> U reduce(U initial, BiFunction<U, T, U> reducer) {
		U result = initial;
		for(T t: this) {
			result = reducer.apply(result, t);
		}
		return result;
	}

}
