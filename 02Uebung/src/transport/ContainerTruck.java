/*
 * Lukas Stallinger k1648098
 */
package transport;

public class ContainerTruck extends Truck{

	public ContainerTruck(String name, int cost, int maxWeight, Location location) {
		super(name, cost, maxWeight, location);
	}
	
	@Override
	public void load(Cargo cargo) throws TransportException {
		if(cargo != null && cargo.getCargoType() == CargoType.SOLID) {
			super.load(cargo);
		} else {
			throw new TransportException(this, "cargoType has to be solid");
		}
	}
	
	@Override
	public String toString() {
		return "ContainerTruck = " + super.toString();
	}
}
