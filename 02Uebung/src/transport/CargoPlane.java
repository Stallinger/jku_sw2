/*
 * Lukas Stallinger k1648098
 */
package transport;

public class CargoPlane extends Transporter {

	private final int costStartLand;
	
	public CargoPlane(String name, int cost, int maxWeight, Location location) {
		this(name, cost, maxWeight, location, 6000);
	}
	
	public CargoPlane(String name, int cost, int maxWeight, Location location, int costStartLand) {
		super(name, cost, maxWeight, location);
		this.costStartLand = costStartLand;
	}
	
	@Override
	public double goTo(Location destination) throws TransportException{
		return super.goTo(destination) + costStartLand;
	}
	
	@Override
	public void load(Cargo cargo) throws TransportException {
		if(cargo != null && cargo.getCargoType() == CargoType.SOLID) {
			super.load(cargo);
		} else {
			throw new TransportException(this,"cargoType has to be solid");
		}
	}
	
	@Override
	public String toString() {
		return "CargoPlane = " + super.toString();
	}
}
