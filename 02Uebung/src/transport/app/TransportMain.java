package transport.app;

import inout.*;
import transport.*;
import transport.TransportException;

public class TransportMain {
	
	
	public static void main(String[] args) {
		Location linz  = new Location("Linz", 0, 0, Country.Austria);
		Location paris = new Location("Paris", 300, 400, Country.France);
		Location ny = new Location("NY", 8000, 0, Country.USA);
		
		Transporter plane = new CargoPlane("plane", 1000, 20000, linz);
		Transporter tankTruck = new TankTruck("tanktruck",200,30000, linz);
		Transporter containerTruck = new ContainerTruck("containertruck",300,16000,linz);
		
		Cargo solid_15 = new Cargo(CargoType.SOLID, "solid_15", 15000);
		Cargo liquid_5 = new Cargo(CargoType.LIQUID, "liquid_5", 5000);
		double cost = 0;
		Out.println("---CargoPlane Test-----------------------------------------------------");
		try { 
			cost += plane.goTo(paris);
			Out.println("Plane flight to Paris ok: " + plane.toString());
			plane.load(solid_15);
			Out.println("Loaded solid_15 on plane ok: " + plane.toString());
			cost+= plane.goTo(ny);
			Out.println("Plane flight to NY ok: " + plane.toString());
			plane.unload();
			Out.println("Plane unload ok: " + plane.toString());
		} catch(TransportException e) {
			Out.println("++ERROR++: Unexpected exception: " + e.toString());
		}
		try {
			plane.load(liquid_5);
			Out.println("++ERROR++: Expected exception but load liquid ok: " + plane.toString());
		} catch(TransportException e) {
			Out.println("Expected exception is: " + e.toString());
		}
		Out.println("cost= " + cost);
		
		Out.println("---ContainerTruck Test-------------------------------------------------");
		
		cost = 0;
		try { 
			containerTruck.load(solid_15);
			Out.println("Loaded solid_15 on ContainerTruck ok: " + containerTruck.toString());
			cost += containerTruck.goTo(paris);
			Out.println("ContainerTruck drive to Paris ok: " + containerTruck.toString());
			containerTruck.unload();
			Out.println("ContainerTruck unload ok: " + containerTruck.toString());
		} catch(TransportException e) {
			Out.println("++ERROR++: Unexpected exception: " + e.toString());
		}
		try {
			containerTruck.goTo(ny);
			Out.println("++ERROR++ cannot drive to another continent ok: " + containerTruck.toString());
		} catch(TransportException e) {
			Out.println("Expected exception is: " + e.toString());
		}
		try {
			containerTruck.load(liquid_5);
			Out.println("++ERROR++: Expected exception but load liquid_5 ok: " + containerTruck.toString());
		} catch(TransportException e) {
			Out.println("Expected exception is: " + e.toString());
		}
		Out.println("cost= " + cost);
		
		Out.println("---TankTruck Test-----------------------------------------------------");
		
		cost = 0;
		try { 
			tankTruck.load(liquid_5);
			Out.println("Loaded liquid_5 on Tanktruck ok: " + tankTruck.toString());
			cost += tankTruck.goTo(paris);
			Out.println("Tanktruck drive to Paris ok: " + tankTruck.toString());
			tankTruck.unload();
			Out.println("Tanktruck unload ok: " + tankTruck.toString());
		} catch(TransportException e) {
			Out.println("++ERROR++: Unexpected exception: " + e.toString());
		}
		try {
			tankTruck.goTo(ny);
			Out.println("++ERROR++ cannot drive to another continent ok: " + tankTruck.toString());
		} catch(TransportException e) {
			Out.println("Expected exception is: " + e.toString());
		}
		try {
			tankTruck.load(solid_15);
			Out.println("++ERROR++: Expected exception but load solid_15 ok: " + tankTruck.toString());
		} catch(TransportException e) {
			Out.println("Expected exception is: " + e.toString());
		}
		Out.println("cost= " + cost);
		
	}
}
