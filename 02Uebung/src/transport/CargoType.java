/*
 * Lukas Stallinger k1648098
 */
package transport;

public enum CargoType {
	LIQUID, 
	SOLID
}
