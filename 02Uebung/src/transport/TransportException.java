/*
 * Lukas Stallinger k1648098
 */
package transport;

@SuppressWarnings("serial")
public class TransportException extends Exception {
	
	private final Transporter transporter;
	private final String message;
	
	public TransportException(Transporter transporter) {
			this(transporter, "");
	}
	
	public TransportException(Transporter transporter, String message) {
		this.transporter = transporter;
		this.message = message;
	}
	
	public Transporter getTransporter() {
		return transporter;
	}
	
	@Override
	public String toString() {
		return "transportexception = (" + message + ")";
	}	
}
