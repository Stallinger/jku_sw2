/*
 * Lukas Stallinger k1648098
 */
package transport;

public class TankTruck extends Truck{

	public TankTruck(String name, int cost, int maxWeight, Location location) {
		super(name, cost, maxWeight, location);
	}
	
	@Override
	public void load(Cargo cargo) throws TransportException {
		if(cargo != null && cargo.getCargoType() == CargoType.LIQUID) {
			super.load(cargo);
		} else {
			throw new TransportException(this, "cargoType has to be liquid");
		}
	}
	
	@Override
	public String toString() {
		return "TankTruck = " + super.toString();
	}
}
