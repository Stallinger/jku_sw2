/*
 * Lukas Stallinger k1648098
 */
package transport;

public class Cargo {
	private final CargoType cargoType;
	private final String name;
	private final int weight;
	
	public Cargo(CargoType cargoType, String name, int weight) {
		this.cargoType = cargoType;
		this.name = name;
		this.weight = weight;
	}

	public CargoType getCargoType() {
		return cargoType;
	}

	public String getName() {
		return name;
	}
	
	public int getWeight() {
		return weight;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargoType == null) ? 0 : cargoType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + weight;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (cargoType != other.cargoType)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (weight != other.weight)
			return false;
		return true;
	}
}
