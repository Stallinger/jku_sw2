/*
 * Lukas Stallinger k1648098
 */
package transport;

public abstract class Transporter {
	private final String name;
	private final int cost;
	private final int maxWeight;
	private Location location;
	private Cargo cargo;
	
	Transporter(String name, int cost,int maxWeight, Location location) {
		this.name = name;
		this.cost = cost;
		this.maxWeight = maxWeight;
		this.location = location;
		this.cargo = null;
	}
	
	public double goTo(Location destination) throws TransportException { 
		double sum = getLocation().getDistance(destination) * (double)this.getCost();
		this.location = destination;
		return sum;
	}
	
	public void load(Cargo cargo) throws TransportException {
		if(this.cargo != null || cargo == null || cargo.getWeight() > maxWeight) {
			throw new TransportException(this, "transporter is not empty or cargo too heavy");
		} else {
			this.cargo = cargo;
		}
	}
	
	public Cargo unload() {
		Cargo cargo = this.getCargo();
		this.cargo = null;
		return cargo;
	}
	
	@Override
	public String toString() {
		return "(name=" + getName() + "|euro/km=" + 
				getCost() + "|maxWeight=" + getMaxWeight() + 
				"|location=" + getLocation() + "|cargo=" + getCargo() + ")";
	}

	public String getName() {
		return name;
	}

	public int getCost() {
		return cost;
	}

	public int getMaxWeight() {
		return maxWeight;
	}

	public Location getLocation() {
		return location;
	}

	public Cargo getCargo() {
		return cargo;
	}
}
