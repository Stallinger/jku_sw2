/*
 * Lukas Stallinger k1648098
 */
package transport;

public abstract class Truck extends Transporter {

	Truck(String name, int cost, int maxWeight, Location location) {
		super(name, cost, maxWeight, location);
	}
	
	@Override
	public double goTo(Location destination) throws TransportException {
		if(this.getLocation() != null && this.getLocation().reachableOverland(destination)) {
			return super.goTo(destination);
		} else {
			throw new TransportException(this, "location is not reachable");
		}
	}
}
