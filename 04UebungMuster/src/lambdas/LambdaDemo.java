package lambdas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.swing.JButton;

import inout.In;
import inout.Out;

public class LambdaDemo {

	@FunctionalInterface
	interface Transformer<A, B> {

		B transform(A a);

		default B checkNullAndTransform(A a) {
			if (a == null)
				return null;
			else
				return transform(a);
		}

	}

	public static void main(String[] args) {

		// 1) ---- implementing interfaces by lambdas ---------------------------------------------

		
		
		
//		ActionListener l = e -> {
//			Out.println("Action executed");
//		};

		JButton btn = new JButton("Click");
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Out.print("Clicked");
			}
		});

		btn.addActionListener(e -> Out.print("Clicked")); 

		
		// Iterator<String> it = () -> // not allowed not a SAM interface
		

		Comparator<String> c = (String s1, String s2) -> s1.compareToIgnoreCase(s2);
		List<String> names = new ArrayList<String>();
		names.add("Hans");
		names.add("Susan");
		names.add("Ann");

		Collections.sort(names, c);

		for (String n : names) {
			Out.println(n);
		}

		Collections.sort(names, (self, other) -> self.compareToIgnoreCase(other));

		
		// 2) ---- functional interfaces ----------------------------------------------------------

		Transformer<String, String> trimmer = s -> {
			return s.trim();
		};

		String strg = "  word  ";
		String trimmed = trimmer.transform(strg);
		trimmed = trimmer.checkNullAndTransform(strg);
		Out.println(trimmed);

		strg = null;
		trimmed = trimmer.checkNullAndTransform(strg);
		Out.println(trimmed);

		
		// 3) ---- functional interfaces from java.util.function ----------------------------------

		Function<String, Integer> length = s -> {
			if (s == null)
				return 0;
			else
				return s.length();
		};

		Predicate<String> notEmpty = s -> s.length() != 0;

		Consumer<String> writer = s -> {
			Out.println(s);
		};

		Supplier<String> reader = () -> {
			return In.readLine();
		};
		
		BiFunction<String, String, Integer> sf = 
				(s1, s2) -> (s1 + s2).length(); 

		
		// 4) ---- using the higher order function ----------------------------------------------

		doGetTestProcessUse(
				() -> {
					Out.println("Input string: ");
					return In.readLine();
				}, 
				s -> s.length() != 0, 
				s -> s.length(), 
				i -> Out.println("Length = " + i)
			);

		
		// 5) ---- show that functional interfaces can be implemented like other interfaces -------

		Function<Integer, Integer> summer = new Function<Integer, Integer>() {
			private int sum = 0;

			@Override
			public Integer apply(Integer i) {
				sum += i;
				return sum;
			}

		};
		

		// 6) ------ Show local variables must be effectively final -------------------------------
		
		List<Integer> list = Arrays.asList(1, 2, 3); 
		// this does not work
		int sum = 0;
		//list.forEach(x -> sum1 = sum1 + 1);
		Out.println(sum);
		
		// workaround durch heap Objekt (BoxedInt or Array)
		class BoxedInt {
			int x; 
		}
		BoxedInt bSum = new BoxedInt(); 
		int[] aSum = new int[1];
		list.forEach(x -> {
			bSum.x = bSum.x + x;
			aSum[0] = aSum[0] + x;
		});
		
	}

	// 4) ---- writing a higher order function -------------------------

	static public <A, B> void doGetTestProcessUse(
								Supplier<A> getter, 
								Predicate<A> condition, 
								Function<A, B> processor,
								Consumer<B> user) {
		A a = getter.get();
		if (condition.test(a)) {
			B b = processor.apply(a);
			user.accept(b);
		}
	}
}
