package tram;

import java.time.LocalTime;
import java.util.*;
import tram.Line.LineStop;

public class Tram implements Comparable<Tram> {

	private final Line line;
	private final LocalTime startTime; 
	private final SortedSet<Stop> stops;

	Tram(final Line line, LocalTime startTime) {
		this.line = line;
		this.startTime = startTime;
		this.stops = new TreeSet<>();
		
		for(LineStop ls : line.getStops()) {
			stops.add(new Stop(this,ls.trampStop,startTime.plusMinutes(ls.minutes)));
		}
		
	}

	public LocalTime getStartTime() { return startTime; }
	
	public Line getLine() { return line; }
	
	public SortedSet<Stop> getStops() { return Collections.unmodifiableSortedSet(stops); }
	
	public SortedSet<Stop> getStops(Station station) {
		if(station == null) return null;

		Optional<Stop> stop = this.stops.stream()
			.filter(s -> s.getStation().compareTo(station) == 0)
			.findFirst();
		
		if(stop.isPresent()) {
			return Collections.unmodifiableSortedSet(this.stops.tailSet(stop.get()));
		} else {
			return null;
		}
	}
	
	
	@Override
	public int compareTo(Tram o) {
		int compare = startTime.compareTo(o.getStartTime());
		if(compare == 0) {
			compare = line.number - o.line.number;
		}
		if(compare == 0) {
			compare = line.terminal.compareTo(o.line.terminal);
		}
		return compare;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((line == null) ? 0 : line.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + ((stops == null) ? 0 : stops.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tram))
			return false;
		Tram other = (Tram) obj;
		if (line == null) {
			if (other.line != null)
				return false;
		} else if (!line.equals(other.line))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (stops == null) {
			if (other.stops != null)
				return false;
		} else if (!stops.equals(other.stops))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "line: " + line + " starttime: " + startTime.toString();
	}
}
