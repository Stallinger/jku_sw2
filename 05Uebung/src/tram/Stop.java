package tram;

import java.time.LocalTime;

public class Stop implements Comparable<Stop> {

	private final Tram tram;
	private final Station station;
	private final LocalTime time;

	Stop(final Tram tram, final Station station, final LocalTime time) {
		this.tram = tram;
		this.station = station;
		this.time = time;
		station.add(tram);
	}
	
	public LocalTime getTime() { return time; }
	public Station getStation() { return station; }
	public Tram getTram() {return tram; }


	@Override
	public int compareTo(Stop o) {
		int compare = time.compareTo(o.getTime());
		if(compare == 0) {
			compare = station.compareTo(o.getStation());
		}
		if(compare == 0) {
			compare = tram.compareTo(o.getTram());
		}
		return compare;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((station == null) ? 0 : station.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result + ((tram == null) ? 0 : tram.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Stop))
			return false;
		Stop other = (Stop) obj;
		if (station != other.station)
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (tram == null) {
			if (other.tram != null)
				return false;
		} else if (!tram.equals(other.tram))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "tram: {" + tram + "} station: " + station + " time: " + time;
	}
}
