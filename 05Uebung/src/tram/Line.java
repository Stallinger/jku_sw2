package tram;

import java.util.*;

public class Line {
	
	public static class LineStop implements Comparable<LineStop> {
		
		final Station trampStop;  
		final int minutes;
		
		LineStop(Station trampStop, int minutes) {
			super();
			this.trampStop = trampStop;
			this.minutes = minutes;
		}

		@Override
		public int compareTo(LineStop o) {
			int compare = trampStop.compareTo(o.trampStop);
			if(compare == 0) {
				compare = minutes - o.minutes;
			}
			return compare;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + minutes;
			result = prime * result + ((trampStop == null) ? 0 : trampStop.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof LineStop))
				return false;
			LineStop other = (LineStop) obj;
			if (minutes != other.minutes)
				return false;
			if (trampStop != other.trampStop)
				return false;
			return true;
		}

	}
	
	public static LineStop stop(Station station, int minutes) {
		return new LineStop(station, minutes); 
	}
	
	public final int number; 
	public final Station terminal;
	private final List<LineStop> stops; 

	Line(int number, Station terminal, LineStop... stops)  {
		super();
		this.number = number; 
		this.terminal = terminal; 
		this.stops = new ArrayList<LineStop>();
		this.stops.addAll(Arrays.asList(stops));
	} 
	
	public List<LineStop> getStops(){ return Collections.unmodifiableList(stops); }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + number;
		result = prime * result + ((stops == null) ? 0 : stops.hashCode());
		result = prime * result + ((terminal == null) ? 0 : terminal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Line))
			return false;
		Line other = (Line) obj;
		if (number != other.number)
			return false;
		if (stops == null) {
			if (other.stops != null)
				return false;
		} else if (!stops.equals(other.stops))
			return false;
		if (terminal != other.terminal)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return number + " " + terminal;
	}
}
