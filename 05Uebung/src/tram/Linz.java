package tram;

import static tram.Line.stop;
import static tram.Station.Auwiesen;
import static tram.Station.Bulgariplatz;
import static tram.Station.Dauphinestrasse;
import static tram.Station.Dornach;
import static tram.Station.Ebelsberg;
import static tram.Station.Ennsfeld;
import static tram.Station.Gruendberg;
import static tram.Station.Hauptbahnhof;
import static tram.Station.Kleinmuenchen;
import static tram.Station.Mozartkreuzung;
import static tram.Station.Ontlstrasse;
import static tram.Station.Rudolfstrasse;
import static tram.Station.Scharlinz;
import static tram.Station.Simonystrasse;
import static tram.Station.SolarCity;
import static tram.Station.Universitaet;

import java.time.LocalTime;

public class Linz {
	
	public static Line line1Auwiesen = new Line(1, Auwiesen, 
				stop(Universitaet, 0), stop(Dornach, 2), stop(Gruendberg, 6),
				stop(Ontlstrasse, 9), stop(Rudolfstrasse, 14), stop(Mozartkreuzung, 19), stop(Hauptbahnhof, 24),
				stop(Bulgariplatz, 28), stop(Scharlinz, 33), stop(Kleinmuenchen, 36), stop(Simonystrasse, 37),
				stop(Dauphinestrasse, 39), stop(Auwiesen, 41));
	
	public static Line line1Universitaet = new Line(1, Universitaet, 
				stop(Universitaet, 41), stop(Dornach, 39),
				stop(Gruendberg, 35), stop(Ontlstrasse, 32), stop(Rudolfstrasse, 27), stop(Mozartkreuzung, 22),
				stop(Hauptbahnhof, 17), stop(Bulgariplatz, 13), stop(Scharlinz, 8), stop(Kleinmuenchen, 5),
				stop(Simonystrasse, 4), stop(Dauphinestrasse, 2), stop(Auwiesen, 0));
	
	public static Line line2SolarCity = new Line(2, SolarCity, 
				stop(Universitaet, 0), stop(Dornach, 2),
				stop(Gruendberg, 6), stop(Ontlstrasse, 9), stop(Rudolfstrasse, 14), stop(Mozartkreuzung, 19),
				stop(Hauptbahnhof, 24), stop(Bulgariplatz, 28), stop(Scharlinz, 33), stop(Kleinmuenchen, 36),
				stop(Ebelsberg, 40), stop(Ennsfeld, 45), stop(SolarCity, 51));
	
	public static Line line2Universitaet = new Line(2, Universitaet, 
				stop(Universitaet, 51), stop(Dornach, 49),
				stop(Gruendberg, 45), stop(Ontlstrasse, 42), stop(Rudolfstrasse, 37), stop(Mozartkreuzung, 32),
				stop(Hauptbahnhof, 27), stop(Bulgariplatz, 23), stop(Scharlinz, 18), stop(Kleinmuenchen, 15),
				stop(Ebelsberg, 11), stop(Ennsfeld, 6), stop(SolarCity, 0));
	
	public static LocalTime[] line1AuwiesenSchedule() {
		return new LocalTime[] {LocalTime.of(6, 00), LocalTime.of(6, 30), LocalTime.of(7, 00),
				LocalTime.of(7, 30), LocalTime.of(8, 00), LocalTime.of(8, 30), LocalTime.of(9, 00), LocalTime.of(9, 30),
				LocalTime.of(10, 00), LocalTime.of(10, 30), LocalTime.of(11, 00), LocalTime.of(11, 30),
				LocalTime.of(12, 00), LocalTime.of(12, 30)};
	}
	
	public static LocalTime[] line1UniversitaetSchedule() {
		return new LocalTime[] {LocalTime.of(6, 00), LocalTime.of(6, 30), LocalTime.of(7, 00),
				LocalTime.of(7, 30), LocalTime.of(8, 00), LocalTime.of(8, 30), LocalTime.of(9, 00), LocalTime.of(9, 30),
				LocalTime.of(10, 00), LocalTime.of(10, 30), LocalTime.of(11, 00), LocalTime.of(11, 30),
				LocalTime.of(12, 00), LocalTime.of(12, 30)};
	}
	
	public static LocalTime[] line2SolarCitySchedule() {
		return new LocalTime[] {LocalTime.of(6, 15), LocalTime.of(6, 45), LocalTime.of(7, 15),
				LocalTime.of(7, 45), LocalTime.of(8, 15), LocalTime.of(8, 45), LocalTime.of(9, 15), LocalTime.of(9, 45),
				LocalTime.of(10, 15), LocalTime.of(10, 45), LocalTime.of(11, 15), LocalTime.of(11, 45),
				LocalTime.of(12, 15), LocalTime.of(12, 45)};
	}
	
	public static LocalTime[] line2UniversitaetSchedule() {
		return new LocalTime[] {LocalTime.of(6, 15), LocalTime.of(6, 45), LocalTime.of(7, 15),
				LocalTime.of(7, 45), LocalTime.of(8, 15), LocalTime.of(8, 45), LocalTime.of(9, 15), LocalTime.of(9, 45),
				LocalTime.of(10, 15), LocalTime.of(10, 45), LocalTime.of(11, 15), LocalTime.of(11, 45),
				LocalTime.of(12, 15), LocalTime.of(12, 45)};
	}
	
}
