package tram;

import java.time.LocalTime;
import java.util.*;

public class TramSchedule {
	
	private final Map<String, Line> lines; 
	private final SortedSet<Tram> trams; 
	
	public TramSchedule() {
		lines = new TreeMap<>();
		lines.put("line1Auwiesen", Linz.line1Auwiesen);
		lines.put("line1Universitaet", Linz.line1Universitaet);
		lines.put("line2SolarCity", Linz.line2SolarCity);
		lines.put("line2Universitaet", Linz.line2Universitaet);
		
		trams = new TreeSet<>();
		for(LocalTime startDate : Linz.line1AuwiesenSchedule()) {
			trams.add(new Tram(lines.get("line1Auwiesen"),startDate));
		}
		for(LocalTime startDate : Linz.line1UniversitaetSchedule()) {
			trams.add(new Tram(lines.get("line1Universitaet"),startDate));
		}
		for(LocalTime startDate : Linz.line2SolarCitySchedule()) {
			trams.add(new Tram(lines.get("line2SolarCity"),startDate));
		}
		for(LocalTime startDate : Linz.line2UniversitaetSchedule()) {
			trams.add(new Tram(lines.get("line2Universitaet"),startDate));
		}
		
	}
	
	public Map<String,Line> getLines() {
		return Collections.unmodifiableMap(lines);
	}
	
	public SortedSet<Tram> getTrams() {
		return Collections.unmodifiableSortedSet(trams);
	}
	
}
