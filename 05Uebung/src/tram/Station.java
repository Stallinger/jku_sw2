package tram;

import java.time.LocalTime;
import java.util.*;

public enum Station implements Comparable<Station> {
	
	Universitaet, Dornach, Gruendberg, Ontlstrasse, Rudolfstrasse, Mozartkreuzung, 
	Hauptbahnhof, Bulgariplatz, Scharlinz, Kleinmuenchen, Simonystrasse, Dauphinestrasse, Auwiesen, 
	Ebelsberg, Ennsfeld, SolarCity;

	private final SortedSet<Tram> trams = new TreeSet<>();

	public SortedSet<Tram> getTrams() { return trams; }
	
	public SortedSet<Tram> getTrams(Line line) {
		if(line == null) return null;
		
		SortedSet<Tram> trams = new TreeSet<>();
		this.trams.stream()
			.filter((t) -> (t.getLine() == line))
			.forEach((t) -> trams.add(t));
		
		return trams;
	}
	
	public SortedSet<Tram> getTrams(Line line,LocalTime time) {
		if(line == null || time == null) return null;
		
		SortedSet<Tram> trams = new TreeSet<>();
		for(Tram t : getTrams(line)) {
			t.getStops()
				.stream()
				.filter((s) -> (s.getStation() == this && s.getTime().compareTo(time) > 0))
				.forEach((s) -> { trams.add(t); });
		}
		return trams;
	}
	
	void add(Tram tram) {
		if(tram != null) {
			trams.add(tram);
		}
	}
}
