package tram.app;

import tram.*;

import java.time.LocalTime;

import inout.*;

public class TramsTest {

	public static void main(String[] args) {
		TramSchedule ts = new TramSchedule();		
		
		
		Tram tram = Station.Mozartkreuzung.getTrams(ts.getLines().get("line2SolarCity"),
				LocalTime.of(12,00)).first();
		
		Out.println(tram.toString());
		Out.println("\t" + tram.getStops(Station.Mozartkreuzung).first());
		
		Out.println("\n");
		
		for(Tram t : Station.Bulgariplatz.getTrams(ts.getLines().get("line2Universitaet"),
				LocalTime.of(12,30))) {
			Out.println(t);
			for(Stop s : t.getStops(Station.Bulgariplatz)) {
				Out.println("\t" + s.toString());
			}
		}
		
		Out.println("\n");
		
		for(Tram t : Station.Ontlstrasse.getTrams(ts.getLines().get("line1Auwiesen"),
				LocalTime.of(12,20))) {
			Out.println(t);
			for(Stop s : t.getStops(Station.Ontlstrasse)) {
				Out.println("\t" + s.toString());
			}
		}
	}

}
